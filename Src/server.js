'use strict'

const variables = require('./consts/variables');
const http = require('http');
const app = require('./configuration/app');
const debug = require('debug')('nodestr:server');

app.set('port', variables.mongoConnection.port);

const server = http.createServer(app);

server.listen(variables.mongoConnection.port, () => {
    console.info(`Api Node rodando na porta: ${variables.mongoConnection.port}`);
});

server.on('listening', onListening);

function onListening(){
    const address = server.address;
    const bind = typeof address === 'string' ? `bind ${address}` : `bind ${address}`;

    debug(`Listening on ${bind}`);
}