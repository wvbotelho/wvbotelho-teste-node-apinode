'use strict'

const express = require('express');
const router = express.Router();
const productController = require('../controllers/product-controller');

let controller = new productController();

router.get('/', controller.get);

router.get('/:artigoId', controller.getById);

router.get('/tags/:tag', controller.getByTag);

router.post('/', controller.create);

router.put('/:artigoId', controller.update);

router.delete('/:artigoId', controller.delete);

module.exports = router;