'use strict'

const express = require('express');

const app = express();
const router = express.Router();

router.get('/', (request, response, next) => {
    response.status(200).send({ title: "Api MongoDB", version: "0.0.2" });
});

module.exports = router;