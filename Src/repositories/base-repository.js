'use strict';

const mongoose = require('mongoose');

class BaseRepository {
    constructor(model) {
        this._model = mongoose.model(model);
    }

    get model() {
        return this._model;
    }

    async getAll() {
        return await this._model.find();
    }

    async getAll(conditions, fields = '') {
        return await this._model.find(conditions, fields);
    }

    async get(conditions, fields = '') {
        return await this._model.findOne(conditions, fields);
    }

    async createOne(model) {
        let modelo = new this._model(model);

        await modelo.save();
    }

    async updateOne(conditions, data) {
        await this._model.updateOne(conditions, data);
    }

    async deleteOne(conditions) {
        await this._model.deleteOne(conditions);
    }
}

module.exports = BaseRepository;