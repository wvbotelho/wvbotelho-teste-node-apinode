'use strict';

const baseRepository = require('../repositories/base-repository');
const variables = require('../consts/variables');

class ProductRepository extends baseRepository {
    constructor() {
        super(variables.models.product);
    }

    async getAll() {
        return await super.getAll({ status: 'Publicado' }, 'artigoId dataPublicacao conteudo autor categorias tags comentarios');
    }

    async getById(id) {
        return await super.get({ artigoId: id }, 'artigoId dataPublicacao conteudo autor categorias tags comentarios');
    }

    async getByTag(tag) {
        return await super.getAll({ tags: tag });
    }

    async createOne(data) {
        await super.createOne(data);
    }

    async updateOne(id, data) {
        await super.updateOne({ artigoId: id }, { $set: data });
    }

    async deleteOne(id) {
        await super.deleteOne({ artigoId: id });
    }
}

module.exports = ProductRepository;