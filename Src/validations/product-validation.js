'use strict';

const validator = require('fluent-validator');

class ProductValidation {
    constructor() {
        this.errors = [];
    }

    isValid(produto) {
        this.isValidProperties(produto);

        return this.errors.length == 0;
    }

    isValidProperties(produto) {
        let validation = validator()
            .validate(produto).isNotNull().or.isNotUndefined()
            .validate(produto.autor).isNotEmpty()
            .validate(produto.categorias).isNotEmpty()
            .validate(produto.tags).isNotEmpty()
            .validate(produto.conteudo).isNotUndefined().and.isNotEmpty();

        this.getErrors(validation.getErrors());
    }

    getErrors(array) {
        array.forEach(teste => {
            this.errors.push({ message: teste.message });
        });
    }
}

module.exports = ProductValidation;