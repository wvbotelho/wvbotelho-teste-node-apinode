'use strict';

const productValidation = require('../validations/product-validation');
const productRepository = require('../repositories/product-repository');

class ProductController {
    constructor() {
    }

    async get(request, response, next) {
        try {
            let produtos = await new productRepository().getAll();

            return response.status(200).send(produtos);
        }
        catch (error) {
            return response.status(400).send({ error: `Deu ruim: ${error}` });
        }
    }

    async getById(request, response, next) {
        try {
            let produto = await new productRepository().getById(request.params.artigoId);

            return response.status(200).send(produto);
        }
        catch (error) {
            return response.status(400).send({ error: `Deu ruim: ${error}` });
        }
    }

    async getByTag(request, response, next) {
        try {
            let produtos = await new productRepository().getByTag(request.params.tag);

            return response.status(200).send(produtos);
        }
        catch (error) {
            return response.status(400).send({ error: `Deu ruim: ${error}` });
        }
    }

    async create(request, response, next) {
        try {
            let validation = new productValidation();

            if (!validation.isValid(request.body)) {
                return response.status(400).send({ errors: validation.errors });
            }

            let produto = await new productRepository().createOne(request.body);

            return response.status(201).send({ message: 'Produto cadastrado com sucesso!', data: produto });
        }
        catch (error) {
            return response.status(400).send({ message: 'Falha ao cadastrar o produto.', data: error });
        }
    }

    async update(request, response, next) {
        try {
            await new productRepository().updateOne(request.params.artigoId, {
                conteudo: request.body.conteudo,
                url: request.body.url,
                status: "Atualizado"
            });

            return response.status(200).send({ message: 'Produto atualizado com sucesso!' });
        }
        catch (error) {
            return response.status(400).send({ message: 'Falha ao cadastrar o produto.', data: error });
        }
    }

    async delete(request, response, next) {
        try {
            await new productRepository().deleteOne(request.params.artigoId);

            return response.status(200).send({ message: `Produto ${request.params.artigoId} removido com sucesso!` });
        }
        catch (error) {
            return response.status(400).send({ message: 'Falha ao remover o produto.', data: error });
        }
    }
}

module.exports = ProductController;

// exports.get = (request, response, next) => {
//     product.find({ status: 'Publicado' }, 'artigoId dataPublicacao conteudo autor categorias tags comentarios')
//         .then(data => {
//             response.status(200).send(data);
//         })
//         .catch(error => {
//             response.status(400).send({ error: `Deu ruim: ${error}` });
//         });
// }

// exports.getById = (request, response, next) => {
//     product.findOne({ artigoId: request.params.artigoId }, 'artigoId dataPublicacao conteudo autor categorias tags comentarios')
//     .then(data => {
//         response.status(200).send(data);
//     })
//     .catch(error => {
//         response.status(400).send({ error: `Deu ruim: ${error}` });
//     });
// }

// exports.getByTag = (request, response, next) => {
//     product.find({ tags: request.params.tag }, 'artigoId dataPublicacao conteudo autor categorias tags comentarios')
//     .then(data => {
//         response.status(200).send(data);
//     })
//     .catch(error => {
//         response.status(400).send({ error: `Deu ruim: ${error}` });
//     });
// }

// exports.post = (request, response, next) => {
//     let produto = new product(request.body);

//     if (!validator.isProductValid(produto)){
//         return response.status(400).send({ errors: validator.errors });
//     }

//     produto.save()
//         .then(data => {
//             response.status(201).send({ message: 'Produto cadastrado com sucesso!', data: data });
//         })
//         .catch(error => {
//             response.status(400).send({ message: 'Falha ao cadastrar o produto.', data: error });
//         });
// }

// exports.put = (request, response, next) => {
//     product.updateOne({ artigoId: request.params.artigoId }, {
//         $set: {
//             conteudo: request.body.conteudo,
//             url: request.body.url,
//             status: "Atualizado"
//         }
//     })
//     .then(data => {
//         response.status(200).send({ message: 'Produto atualizado com sucesso!', data: data });
//     })
//     .catch(error => {
//         response.status(400).send({ message: 'Falha ao cadastrar o produto.', data: error });
//     });
// }

// exports.delete = (request, response, next) => {
//     product.deleteOne({ artigoId: request.params.artigoId })
//         .then(data => {
//             response.status(200).send({ message: `Produto ${request.params.artigoId} removido com sucesso!`});
//         })
//         .catch(error => {
//             response.status(400).send({ message: 'Falha ao remover o produto.', data: error });
//         });
// }