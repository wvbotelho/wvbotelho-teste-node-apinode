'use strict'

const guid = require('mongoose-guid');
const mongoose = require('mongoose');
const variables = require('../../consts/variables');

const schema = mongoose.Schema;

const categoriaDto = new schema({
    categoriaId: typeof guid,
    nome: String
},
{ versionKey: false, _id: false });

const comentarioDto = new schema({
    _id: { type: schema.Types.ObjectId, auto: true, alias: 'internalId' },
    comentarioId: { type: typeof guid, default: guid.value, unique: true },
    texto: { type: String, required: [true, 'texto é obrigatório'] },
    dataPublicacao: { type: Date, default: Date.now }
},
{ versionKey: false, _id: false });

const productSchema = new schema({
    _id: { type: schema.Types.ObjectId, auto: true, alias: 'internalId' },
    artigoId: { type: typeof guid, unique: true, default: guid.value },
    dataPublicacao: { type: Date, default: Date.now },
    conteudo : { type: String, require: [true, 'conteudo é obrigatório'] },
    autor: {
        autorId: typeof guid,
        nome: String
    },
    status: { type: String, required: [true, 'status é obrigatório'], enum: ['Rascunho', 'Publicado', 'Atualizado', 'Deletado'] },
    url: { type: String, require: [true, 'url é obrigatório'] },
    categorias: [categoriaDto],
    tags: [String],
    comentarios: [comentarioDto],
    versionSchema: { type: Number, default: 1 }
},
{ versionKey: false, _id: false });

module.exports = mongoose.model(variables.models.product, productSchema, variables.models.product);