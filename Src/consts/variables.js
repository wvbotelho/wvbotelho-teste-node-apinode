'use strict'

const application = {
    mongoConnection: {
        port: process.env.PORT || 7010
    },
    models: {
        product: 'product'
    }
}

module.exports = application;