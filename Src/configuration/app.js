'use strict';

const express = require('express');
const parser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const router = express.Router();

//Conecta ao banco
mongoose.connect('mongodb://localhost:32768/api_node', { useNewUrlParser: true, useCreateIndex: true });

let database = mongoose.connection;

database.on('error', console.error.bind(console, 'erro ao conectar ao MongoDB:'));
database.once('open', () => {
    console.log('conectado na database api_node');
})

//Carrega as models
const product = require('../models/schemas/product-schema');

//carrega as rotas
const indexRoute = require('../routes/index-route');
const productRoute = require('../routes/product-route');

//configura toda a resposta de uma requisição no formato json
app.use(parser.json());
app.use(parser.urlencoded({ extended: false }));

app.use('/', indexRoute);
app.use('/products', productRoute);

module.exports = app;